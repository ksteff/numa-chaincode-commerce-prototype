package com.bitt.numa.chaincode.commerce

import com.owlike.genson.Genson
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.hyperledger.fabric.contract.Context
import org.hyperledger.fabric.shim.ChaincodeException
import org.hyperledger.fabric.shim.ChaincodeStub
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.math.BigDecimal
import java.util.*

class NumaCommerceTest {

  @Nested
  inner class CreateStockWalletTests {

    @Test
    internal fun `creating a new wallet should return the new wallet with a balance of zero`() {
      val numaCommerce = NumaCommerce()
      val stub = mockk<ChaincodeStub>(relaxed = true)
      every { stub.getStringState(numaCommerce.stockWalletKey) } returns ""
      every { stub.creator } returns "admin".toByteArray()
      val ctx = mockk<Context>()
      every { ctx.stub } returns stub
      val (owner, currency, balance) = numaCommerce.createStockWallet(ctx, "DXCD")
      assertThat(owner).isEqualTo("admin")
      assertThat(currency).isEqualTo("DXCD")
      assertThat(balance).isEqualTo(BigDecimal.ZERO)
      verify {
        stub.putStringState(numaCommerce.stockWalletKey, """{"balance":0,"currency":"DXCD","owner":"admin"}""")
      }
    }

    @Test
    internal fun `creating a wallet when it already exists should throw an error`() {
      val numaCommerce = NumaCommerce()
      val stub = mockk<ChaincodeStub>(relaxed = true)
      val walletJson = """{"balance":0,"currency":"DXCD","owner":"admin"}"""
      every { stub.getStringState(numaCommerce.stockWalletKey) } returns walletJson
      val ctx = mockk<Context>()
      every { ctx.stub } returns stub
      val chaincodeException = assertThrows<ChaincodeException> { numaCommerce.createStockWallet(ctx, "DXCD") }
      assertThat(chaincodeException.payload).isEqualTo("WALLET_ALREADY_EXISTS".toByteArray())
      verify(exactly = 0) {
        stub.putStringState(numaCommerce.stockWalletKey, walletJson)
      }
    }
  }

  @Nested
  inner class GetStockWalletTests {

    @Test
    internal fun `fetching the stock wallet after it has been created should return the wallet`() {
      val numaCommerce = NumaCommerce()
      val stub = mockk<ChaincodeStub>(relaxed = true)
      val walletJson = """{"balance":15000,"currency":"DXCD","owner":"admin"}"""
      every { stub.getStringState(numaCommerce.stockWalletKey) } returns walletJson
      val ctx = mockk<Context>()
      every { ctx.stub } returns stub
      val (owner, currency, balance) = numaCommerce.getStockWallet(ctx)
      assertThat(owner).isEqualTo("admin")
      assertThat(currency).isEqualTo("DXCD")
      assertThat(balance).isEqualTo(BigDecimal("15000"))
    }

    @Test
    internal fun `fetching the stock wallet when it does not exist should throw an error`() {
      val numaCommerce = NumaCommerce()
      val stub = mockk<ChaincodeStub>(relaxed = true)
      every { stub.getStringState(numaCommerce.stockWalletKey) } returns ""
      val ctx = mockk<Context>()
      every { ctx.stub } returns stub
      val chaincodeException = assertThrows<ChaincodeException> { numaCommerce.getStockWallet(ctx) }
      assertThat(chaincodeException.payload).isEqualTo("WALLET_NOT_FOUND".toByteArray())
    }
  }

  @Nested
  inner class AddMoneyToStockWalletTests {

    @Test
    internal fun `adding money to a stock wallet with a new transaction id should increase balance by new amount`() {
      val transactionId = UUID.randomUUID().toString()
      val currency = "DXCD"
      val amount = BigDecimal("5000")
      val numaCommerce = NumaCommerce()
      val stub = mockk<ChaincodeStub>(relaxed = true)
      val walletJson = """{"balance":15000,"currency":"DXCD","owner":"admin"}"""
      val genson = Genson()
      every { stub.getStringState(numaCommerce.stockWalletKey) } returns walletJson
      every { stub.getStringState(transactionId) } returns ""
      every { stub.creator } returns "admin".toByteArray()
      val ctx = mockk<Context>()
      every { ctx.stub } returns stub
      val ledgerTransaction = numaCommerce.addMoneyToStockWallet(
        ctx = ctx,
        transactionId = transactionId,
        currency = currency,
        amount = amount
      )
      assertThat(ledgerTransaction)
        .hasFieldOrPropertyWithValue("id", transactionId)
        .hasFieldOrPropertyWithValue("currency", currency)
        .hasFieldOrPropertyWithValue("amount", amount)
        .hasFieldOrPropertyWithValue("previousBalance", BigDecimal("15000"))
        .hasFieldOrPropertyWithValue("newBalance", BigDecimal("20000"))
        .hasFieldOrPropertyWithValue("senderType", WalletType.MINTING)
        .hasFieldOrPropertyWithValue("sender", "admin")
        .hasFieldOrPropertyWithValue("receiverType", WalletType.STOCK_WALLET)
        .hasFieldOrPropertyWithValue("receiver", "admin")
      assertThat(ledgerTransaction.receiptId).isNotNull()
      verify {
        stub.putStringState(numaCommerce.stockWalletKey, """{"balance":20000,"currency":"DXCD","owner":"admin"}""")
        stub.putStringState(transactionId, genson.serialize(ledgerTransaction))
      }
    }

    @Test
    internal fun `adding money to a stock wallet that does not exist should fail`() {
      val transactionId = UUID.randomUUID().toString()
      val currency = "DXCD"
      val amount = BigDecimal("5000")
      val numaCommerce = NumaCommerce()
      val stub = mockk<ChaincodeStub>(relaxed = true)
      every { stub.getStringState(numaCommerce.stockWalletKey) } returns ""
      val ctx = mockk<Context>()
      every { ctx.stub } returns stub
      val chaincodeException = assertThrows<ChaincodeException> {
        numaCommerce.addMoneyToStockWallet(
          ctx = ctx,
          transactionId = transactionId,
          currency = currency,
          amount = amount
        )
      }
      assertThat(chaincodeException.message).isEqualTo("Stock Wallet has not been created")
      assertThat(chaincodeException.payload).isEqualTo("WALLET_NOT_FOUND".toByteArray())
    }

    @Test
    internal fun `adding an amount of zero to a stock wallet should fail`() {
      val numaCommerce = NumaCommerce()
      val stub = mockk<ChaincodeStub>(relaxed = true)
      every { stub.getStringState(numaCommerce.stockWalletKey) } returns """{"balance":15000,"currency":"DXCD","owner":"admin"}"""
      val ctx = mockk<Context>()
      every { ctx.stub } returns stub
      val chaincodeException = assertThrows<ChaincodeException> {
        numaCommerce.addMoneyToStockWallet(
          ctx = ctx,
          transactionId = UUID.randomUUID().toString(),
          currency = "DXCD",
          amount = BigDecimal.ZERO
        )
      }
      assertThat(chaincodeException.message).isEqualTo("Amount must be greater than zero")
      assertThat(chaincodeException.payload).isEqualTo("INVALID_AMOUNT".toByteArray())
    }

    @Test
    internal fun `adding a negative amount to a stock wallet should fail`() {
      val numaCommerce = NumaCommerce()
      val stub = mockk<ChaincodeStub>(relaxed = true)
      every { stub.getStringState(numaCommerce.stockWalletKey) } returns """{"balance":15000,"currency":"DXCD","owner":"admin"}"""
      val ctx = mockk<Context>()
      every { ctx.stub } returns stub
      val chaincodeException = assertThrows<ChaincodeException> {
        numaCommerce.addMoneyToStockWallet(
          ctx = ctx,
          transactionId = UUID.randomUUID().toString(),
          currency = "DXCD",
          amount = BigDecimal("-5000")
        )
      }
      assertThat(chaincodeException.message).isEqualTo("Amount must be greater than zero")
      assertThat(chaincodeException.payload).isEqualTo("INVALID_AMOUNT".toByteArray())
    }

    @Test
    internal fun `adding money with an existing transaction id should fail`() {
      val transactionId = UUID.randomUUID().toString()
      val currency = "DXCD"
      val amount = BigDecimal("5000")
      val numaCommerce = NumaCommerce()
      val stub = mockk<ChaincodeStub>(relaxed = true)
      val walletJson = """{"balance":15000,"currency":"DXCD","owner":"admin"}"""
      val genson = Genson()
      val ledgerTransaction = LedgerTransaction(
        id = transactionId,
        amount = amount,
        currency = currency,
        newBalance = BigDecimal("20000"),
        previousBalance = BigDecimal("15000"),
        sender = "admin",
        senderType = WalletType.MINTING,
        receiver = "admin",
        receiverType = WalletType.STOCK_WALLET
      )
      every { stub.getStringState(numaCommerce.stockWalletKey) } returns walletJson
      every { stub.getStringState(transactionId) } returns genson.serialize(ledgerTransaction)
      val ctx = mockk<Context>()
      every { ctx.stub } returns stub
      val chaincodeException = assertThrows<ChaincodeException> {
        numaCommerce.addMoneyToStockWallet(
          ctx = ctx,
          transactionId = transactionId,
          currency = currency,
          amount = amount
        )
      }
      assertThat(chaincodeException.payload).isEqualTo("TRANSACTION_ALREADY_EXISTS".toByteArray())
    }
  }

}
