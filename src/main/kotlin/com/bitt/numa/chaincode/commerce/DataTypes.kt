package com.bitt.numa.chaincode.commerce

import com.owlike.genson.annotation.JsonProperty
import org.hyperledger.fabric.contract.annotation.DataType
import org.hyperledger.fabric.contract.annotation.Property
import java.math.BigDecimal
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*

@DataType
data class Approval(
  @JsonProperty("owner")
  @Property
  val owner: String,

  @JsonProperty("approvalDate")
  @Property
  val approvalDate: ZonedDateTime
)

@DataType
data class LedgerTransaction(
  @JsonProperty("id")
  @Property
  val id: String,

  @JsonProperty("amount")
  @Property
  val amount: BigDecimal,

  @JsonProperty("currency")
  @Property
  val currency: String,

  @JsonProperty("transactionDate")
  @Property
  val transactionDate: ZonedDateTime = ZonedDateTime.now(ZoneId.of("UTC")),

  @JsonProperty("receiptId")
  @Property
  val receiptId: String = UUID.randomUUID().toString(),

  @JsonProperty("sender")
  @Property
  val sender: String,

  @JsonProperty("sender_type")
  @Property
  val senderType: WalletType,

  @JsonProperty("receiver")
  @Property
  val receiver: String,

  @JsonProperty("receiver_type")
  @Property
  val receiverType: WalletType,

  @JsonProperty("approvals")
  @Property
  val approvals: List<Approval> = listOf(),

  @JsonProperty("signature")
  @Property
  val signature: String? = null,

  @JsonProperty("previous_balance")
  @Property
  val previousBalance: BigDecimal,

  @JsonProperty("new_balance")
  @Property
  val newBalance: BigDecimal
)

@DataType
data class Wallet(
  @JsonProperty("owner")
  @Property
  val owner: String,

  @JsonProperty("currency")
  @Property
  val currency: String,

  @JsonProperty("balance")
  @Property
  val balance: BigDecimal = BigDecimal.ZERO
)

enum class WalletType {
  MINTING,
  STOCK_WALLET,
  TREASURY_WALLET,
  BRANCH_WALLET,
  COMMERCE_WALLET
}
