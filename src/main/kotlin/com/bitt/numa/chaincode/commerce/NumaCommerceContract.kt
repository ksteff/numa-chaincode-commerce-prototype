package com.bitt.numa.chaincode.commerce

import com.bitt.numa.chaincode.commerce.ContractErrors.*
import com.owlike.genson.Genson
import org.hyperledger.fabric.contract.Context
import org.hyperledger.fabric.contract.ContractInterface
import org.hyperledger.fabric.contract.annotation.Contract
import org.hyperledger.fabric.contract.annotation.Default
import org.hyperledger.fabric.contract.annotation.Info
import org.hyperledger.fabric.contract.annotation.Transaction
import org.hyperledger.fabric.shim.ChaincodeException
import org.hyperledger.fabric.shim.ChaincodeStub
import java.math.BigDecimal

/**
 * Smart Contract for governing monetary flow
 */
@Contract(
  name = "NumaCommerce",
  info = Info(
    title = "Commerce contract",
    description = "Smart contract for governing monetary flow",
    version = "0.0.1-SNAPSHOT"
  )
)
@Default
class NumaCommerce : ContractInterface {

  private val genson = Genson()
  val stockWalletKey = "stock"

  @Transaction
  fun createStockWallet(ctx: Context, currency: String): Wallet {
    val stub = ctx.stub
    val walletState = stub.getStringState(stockWalletKey)
    if (walletState.isNotEmpty()) {
      throw ChaincodeException("Stock Wallet already exists", WALLET_ALREADY_EXISTS.toString())
    }
    val stockWallet = Wallet(
      owner = String(stub.creator),
      currency = currency
    )
    stub.putStringState(stockWalletKey, genson.serialize(stockWallet))
    return stockWallet
  }

  @Transaction
  fun getStockWallet(ctx: Context): Wallet {
    val stub = ctx.stub
    return fetchStockWallet(stub)
  }

  @Transaction
  fun addMoneyToStockWallet(
    ctx: Context,
    transactionId: String,
    currency: String,
    amount: BigDecimal
  ): LedgerTransaction {
    val stub = ctx.stub
    val wallet = fetchStockWallet(stub)
    verifyPositiveAmount(amount)
    verifyNewTransaction(stub, transactionId)
    val newBalance = wallet.balance.add(amount)
    val ledgerTransaction = LedgerTransaction(
      id = transactionId,
      sender = String(stub.creator),
      senderType = WalletType.MINTING,
      receiver = wallet.owner,
      receiverType = WalletType.STOCK_WALLET,
      currency = currency,
      amount = amount,
      previousBalance = wallet.balance,
      newBalance = newBalance
    )
    stub.putStringState(transactionId, genson.serialize(ledgerTransaction))
    val newWallet = wallet.copy(balance = newBalance)
    stub.putStringState(stockWalletKey, genson.serialize(newWallet))
    return ledgerTransaction
  }

  private fun verifyNewTransaction(
    stub: ChaincodeStub,
    transactionId: String
  ) {
    val transactionState = stub.getStringState(transactionId)
    if (transactionState.isNotEmpty()) {
      throw ChaincodeException("Transaction with $transactionId already exists", TRANSACTION_ALREADY_EXISTS.toString())
    }
  }

  private fun fetchStockWallet(stub: ChaincodeStub): Wallet {
    val walletState = stub.getStringState(stockWalletKey)
    if (walletState.isEmpty()) {
      throw ChaincodeException("Stock Wallet has not been created", WALLET_NOT_FOUND.toString())
    }
    return genson.deserialize(walletState, Wallet::class.java)
  }

  private fun verifyPositiveAmount(amount: BigDecimal) {
    if (amount <= BigDecimal.ZERO) {
      throw ChaincodeException("Amount must be greater than zero", INVALID_AMOUNT.toString())
    }
  }

}

enum class ContractErrors {
  INVALID_AMOUNT,
  TRANSACTION_ALREADY_EXISTS,
  WALLET_ALREADY_EXISTS,
  WALLET_NOT_FOUND
}
