plugins {
    id("com.github.johnrengelman.shadow") version "2.0.4"
    id("java-library")
    kotlin("jvm") version "1.3.61"
}

group = "com.bitt"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
    maven("https://jitpack.io")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.hyperledger.fabric-chaincode-java:fabric-chaincode-shim:1.4.3")
    implementation("com.owlike:genson:1.5")
    testImplementation("org.junit.jupiter:junit-jupiter:5.4.2")
    testImplementation("org.assertj:assertj-core:3.11.1")
    testImplementation("io.mockk:mockk:1.9.3.kotlin12")
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    shadowJar {
        baseName = "chaincode"
        version = null
        classifier = null
        manifest {
            attributes["Main-Class"] = "org.hyperledger.fabric.contract.ContractRouter"
        }
    }
}

/*
*/

